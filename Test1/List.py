class List: 
    """Class that implements classic one-way list """

    def __init__(self, value='', next_=None, prev=None):
        self._value, self._next_ = value, next_
        self._pointer = self

    def __str__(self):
        pointer = self._next_
        result = str(self._value)
        while pointer != None:
            result += ' ' + str(pointer._value)
            pointer = pointer._next_
        return result

    def append(self,tail):
        pointer = self
        while (pointer._next_ != None):
            pointer = pointer._next_
        pointer._next_ = List(tail)

    def __iadd__(self, other):
        if isinstance(other, List):
            pointer = other
            while (pointer._next_ != None):
                self.append(pointer._value)
                pointer = other._next_
            self.append(pointer._value)    
        elif isinstance(other, list):
            for item in other:
                self.append(item)
        else:
            raise NotImplementedError
        return self

    def print(self):
        '''Вывод списка. Последний элемент в списке печатается с переносом строки.'''
        pointer=self
        while (pointer._next_ != None):
            print(pointer._value, end=' ')
            pointer = pointer._next_

        print(pointer._value)



    def __iter__(self):
        return self
    
    def __next__(self):
        if self._pointer == None:
            raise StopIteration
        else:
            current_value = self._pointer._value
            self._pointer = self._pointer._next_
            return current_value

    def print_reversed(self):
        if self._next_ == None:
            print(self._value)
        else: 
            self._next_.print_reversed()
            print(self._value)
    


if  __name__ == '__main__' :
   
    list_ = List(value=1, next_=List(value=2, next_=List(value=3))) 
    list_.print()   # output: 1 2 3 
    
    list_.append(4) 
    list_.print()   # output: 1 2 3 4 
 
    tail = List(value=5, next_=List(value=6)) 
    list_ += tail   # shallow copy, see examples below 
    list_.print()   # output: 1 2 3 4 5 6 
    tail._value = 0 
    tail.print()    # output: 0 6; element 5 in tail is changed 
    list_.print()   # output: 1 2 3 4 5 6; element 5 in list_ is NOT changed 
 
    list_ += [7, 8] 
    list_.print()   # output: 1 2 3 4 5 6 7 8 
    for elem in list_:     
        print(2 ** elem , end = ' ')  
    print('')  # output: 2 4 8 16 32 64 128 256 
    list_.print_reversed()  # output: 8 7 6 5 4 3 2 1 

    empty_list = List()
    empty_list.print()
       
    list_with_single_none_element = List(None)
    list_with_single_none_element.print()
    
        