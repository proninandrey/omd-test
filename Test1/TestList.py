import unittest
import io
import sys

from List import List

class ListTest(unittest.TestCase):
    
    
    def test_creation_and_print(self):
        list_ = List(value=1, next_=List(value=2, next_=List(value=3))) 
        self.assertEqual(str(list_),'1 2 3')
        
        output = io.StringIO()
        sys.stdout = output
        list_.print()
        sys.stdout = sys.__stdout__
        self.assertEqual(output.getvalue(), '1 2 3\n')

    def test_append(self):
        list_ = List(value=1, next_=List(value=2, next_=List(value=3)))
        list_.append(4)
        self.assertEqual(str(list_),'1 2 3 4')

    def test_radd(self):
        list_ = List(value=1)
        list_.append(2)
        list_.append(3)
        list_.append(4)
        tail = List(value=5, next_=List(value=6))
        list_ += tail
        self.assertEqual(str(list_), '1 2 3 4 5 6')
        tail._value = 0
        self.assertEqual(str(list_), '1 2 3 4 5 6')
        self.assertEqual(str(tail), '0 6')

        list_ += [7,8] 
        self.assertEqual(str(list_), '1 2 3 4 5 6 7 8')
                
    def test_pow(self):
        output = io.StringIO()
        sys.stdout = output
        list_ = List(value=1)
        list_ += [2,3,4,5,6,7,8]
        for elem in list_:
            print(2 ** elem, end=' ')
        
        sys.stdout = sys.__stdout__
        self.assertEqual(output.getvalue(), '2 4 8 16 32 64 128 256 ')

    def test_reversed_print(self):
        list_ = List(value=1, next_=List(value=2, next_=List(value=3))) 
        output = io.StringIO()
        sys.stdout = output
        list_.print_reversed()
        sys.stdout = sys.__stdout__
        self.assertEqual(output.getvalue(), '3\n2\n1\n')   

    def test_empty_list(self):
        empty_list = List()
        output = io.StringIO()
        sys.stdout = output
        empty_list.print()
        sys.stdout = sys.__stdout__
        self.assertEqual(output.getvalue(), '\n')

    def test_none_value(self):
        one_none_value_list = List(None)
        output = io.StringIO()
        sys.stdout = output
        one_none_value_list.print()
        sys.stdout = sys.__stdout__
        self.assertEqual(output.getvalue(), 'None\n')


if __name__ == '__main__':
    unittest.main()

