CREATE TABLE POST_STATS_V2
as (
  
SELECT
  post_id,
  likes,
  views,
  shares,
  MIN(dttm) as effective_from_dttm,
  effective_to_dttm
FROM (
  SELECT 
    t1.*,
    MIN(COALESCE(t2.dttm, CAST('9999-12-31 23:59:59' as TIMESTAMP))) as effective_to_dttm
  FROM post_stats ps1
  LEFT JOIN post_stats ps2
  ON 1=1
    AND ps1.post_id = ps2.post_id
    AND NOT (
        ps1.likes = ps2.likes
        AND ps1.views = ps2.views
        AND ps1.shares = ps2.shares
      )
    AND ps1.dttm < ps2.dttm
  GROUP BY ps1.post_id, ps1.likes, ps1.views, ps1.shares, ps1.dttm
)pre_sc2
GROUP BY
  post_id,
  likes,
  views,
  shares,
  effective_to_dttm

) WITH DATA PRIMARY INDEX(post_id, effective_from_dttm, effective_to_dttm);